import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { config } from "../../hostinfo";

export const VEHICLES_API_REDUCER_KEY = "vehiclesApi";

export type VehicleSearchArgs = {
  readonly make: string;
  readonly model: string;
};

export const vehiclesApi = createApi({
  reducerPath: VEHICLES_API_REDUCER_KEY,
  baseQuery: fetchBaseQuery({ baseUrl: config.api.baseUrl }),
  endpoints: (builder) => ({
    getVehiclesByMakeAndModel: builder.query<string[], VehicleSearchArgs>({
      query: (args) => `/vehicles?make=${args.make}&model=${args.model}`
    })
  }),
  refetchOnMountOrArgChange: 60
});

export const { useGetVehiclesByMakeAndModelQuery } = vehiclesApi;
