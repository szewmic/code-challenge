import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { config } from "../../hostinfo";

export const MAKES_API_REDUCER_KEY = "makesApi";

export const makesApi = createApi({
  reducerPath: MAKES_API_REDUCER_KEY,
  baseQuery: fetchBaseQuery({ baseUrl: config.api.baseUrl }),
  endpoints: (builder) => ({
    getMakes: builder.query<string[], null>({
      query: () => {
        return "makes";
      }
    })
  }),
  refetchOnMountOrArgChange: 60
});

export const { useGetMakesQuery } = makesApi;
