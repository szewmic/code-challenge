import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { config } from "../../hostinfo";

export const MODELS_API_REDUCER_KEY = "modelsApi";

export const modelsApi = createApi({
  reducerPath: MODELS_API_REDUCER_KEY,
  baseQuery: fetchBaseQuery({ baseUrl: config.api.baseUrl }),
  endpoints: (builder) => ({
    getModelsByMake: builder.query<string[], string>({
      query: (args) => `/models?make=${args}`
    })
  }),
  refetchOnMountOrArgChange: 60
});

export const { useGetModelsByMakeQuery } = modelsApi;
