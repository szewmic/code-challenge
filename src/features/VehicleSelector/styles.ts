import Styled from "styled-components";

export const VehicleSelectorContainer = Styled.div`
height: 100%;
@media (min-width: 34em) {
  display: flex;
}
`;

export const MakesListContainer = Styled.div`
@media (min-width: 34em) {
  flex: 1 0 25%;
}
`;

export const VehiclesGalleryContainer = Styled.div`
@media (min-width: 34em) {
  flex: 1 0 75%;
}

@media (max-width: 34em) {
  margin-top: 10px;
}
`;
