import React from "react";
import { Gallery, Info, Retry } from "../../../../common/components/index";
import { RootState, useTypedSelector } from "../../../../common/store";
import { useSearchVehicles } from "../../hooks/useSearchVehicles";
import { VehicleGalleryItem } from "./VehicleGalleryItem/index";

export interface VehiclesGalleryProps {
  onSelect?: (data: any) => void;
}

const VehiclesGallery = ({ onSelect }: VehiclesGalleryProps) => {
  const make = useTypedSelector(
    (state: RootState) => state.vehicleSelectorSlice.selectedMake
  );
  const model = useTypedSelector(
    (state: RootState) => state.vehicleSelectorSlice.selectedModel
  );

  const {
    data: vehicles,
    isUninitialized,
    isError,
    refetch
  } = useSearchVehicles(make && model ? { make: make, model: model } : null);

  if (isUninitialized) {
    return <Info text="Select make and model first" />;
  }

  if (isError) {
    return <Retry onRetry={refetch} />;
  }

  return (
    <Gallery
      data={vehicles ?? []}
      valueExpr={"model"}
      onSelect={onSelect}
      ItemComponent={VehicleGalleryItem}
    />
  );
};

export default VehiclesGallery;
