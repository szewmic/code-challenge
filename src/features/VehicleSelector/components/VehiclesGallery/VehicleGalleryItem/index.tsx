import { Vehicle } from "../../../models/vehicle";
import { VehicleGalleryItemContainer, VehicleGalleryItemLabel } from "./styles";

interface VehicleGalleryItemProps {
  data: Vehicle;
}
export const VehicleGalleryItem = ({ data }: VehicleGalleryItemProps) => {
  return (
    <VehicleGalleryItemContainer>
      <VehicleGalleryItemLabel>
        {data.make} {data.model}
      </VehicleGalleryItemLabel>
      <VehicleGalleryItemLabel>
        Engine Power PS: {data.enginePowerPS}
      </VehicleGalleryItemLabel>
      <VehicleGalleryItemLabel>
        Engine Power KW {data.enginePowerKW}
      </VehicleGalleryItemLabel>
      <VehicleGalleryItemLabel>
        Fuel Type: {data.fuelType}
      </VehicleGalleryItemLabel>
      <VehicleGalleryItemLabel>
        Body Type: {data.bodyType}
      </VehicleGalleryItemLabel>
      <VehicleGalleryItemLabel>
        Engine Capacity: {data.engineCapacity}
      </VehicleGalleryItemLabel>
    </VehicleGalleryItemContainer>
  );
};
