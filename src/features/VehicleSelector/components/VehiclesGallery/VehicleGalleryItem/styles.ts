import Styled from "styled-components";

export const VehicleGalleryItemContainer = Styled.div`
text-align: center;
`;

export const VehicleGalleryItemLabel = Styled.span`
display: block;
`;
