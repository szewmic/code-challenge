import React, { useCallback } from "react";
import { useGetMakesQuery } from "../../../../api/makes/api";
import { Retry } from "../../../../common/components";
import List from "../../../../common/components/List/index";
import { useTypedDispatch } from "../../../../common/store";
import { setSelectedMake } from "../../slice";
import { ModelsList } from "./ModelsList/index";

export interface MakesListProps {}

const MakesList = (props: MakesListProps) => {
  const dispatch = useTypedDispatch();
  const { data: makes, isError, refetch } = useGetMakesQuery(null);

  const onSelect = useCallback(
    (si) => {
      dispatch(setSelectedMake(si));
    },
    [dispatch]
  );

  if (isError) {
    return <Retry onRetry={refetch}></Retry>;
  }

  return (
    <List
      title="Makes"
      data={makes}
      filteringEnabled={true}
      onSelect={onSelect}
      ItemComponent={ModelsList}
    />
  );
};

export default MakesList;
