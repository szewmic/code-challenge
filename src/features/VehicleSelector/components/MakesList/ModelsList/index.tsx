import { useTypedDispatch } from "../../../../../common/store";
import { useSearchModels } from "../../../hooks/useSearchModels";
import List from "../../../../../common/components/List/index";
import { setSelectedModel } from "../../../slice";
import { useCallback } from "react";
import { Retry } from "../../../../../common/components";

interface ModelsListProps {
  data: any;
}

export const ModelsList = ({ data }: ModelsListProps) => {
  const dispatch = useTypedDispatch();

  const onSelect = useCallback(
    (si) => {
      dispatch(setSelectedModel(si));
    },
    [dispatch]
  );

  const { data: models, isError, refetch } = useSearchModels(data);

  if (isError) {
    return <Retry type="dark" onRetry={refetch} />;
  }
  return <List data={models} onSelect={onSelect} />;
};
