import { Vehicle } from "./models/vehicle";

export interface VehicleSelectorState {
  selectedMake?: string;
  selectedModel?: string;
  selectedVehicle?: Vehicle;
}
