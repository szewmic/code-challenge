import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Vehicle } from "./models/vehicle";
import { VehicleSelectorState } from "./types";

const initialState: VehicleSelectorState = {};

export const vehicleSelectorSlice = createSlice({
  name: "vehicleSelectorSlice",
  initialState,
  reducers: {
    setSelectedMake(state, action: PayloadAction<string | undefined>) {
      state.selectedMake = action.payload;
    },
    setSelectedModel(state, action: PayloadAction<string | undefined>) {
      state.selectedModel = action.payload;
    },
    setSelectedVehicle(state, action: PayloadAction<Vehicle | undefined>) {
      state.selectedVehicle = action.payload;
    }
  }
});

export const {
  setSelectedMake,
  setSelectedModel,
  setSelectedVehicle
} = vehicleSelectorSlice.actions;
const { reducer } = vehicleSelectorSlice;
export default reducer;
