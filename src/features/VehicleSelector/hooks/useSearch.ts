import { useCallback, useEffect } from "react";
import { useTypedDispatch } from "../../../common/store/index";

export const useSearch = (query: any, args: any) => {
  const dispatch = useTypedDispatch();

  const vehicleSearchFn = useCallback(
    (args: any) => {
      dispatch(query.initiate(args));
    },
    [dispatch]
  );

  useEffect(() => {
    if (args) vehicleSearchFn(args);
  }, [args]);

  return query.useQuery(args, {
    skip: args === null
  });
};
