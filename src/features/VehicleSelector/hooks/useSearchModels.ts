import { modelsApi } from "../../../api/models/api";
import { useSearch } from "./useSearch";

export const useSearchModels = (args: string | null) => {
  return useSearch(modelsApi.endpoints.getModelsByMake, args);
};
