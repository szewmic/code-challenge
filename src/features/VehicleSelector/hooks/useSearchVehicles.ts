import { vehiclesApi, VehicleSearchArgs } from "../../../api/vehicles/api";
import { useSearch } from "./useSearch";

export const useSearchVehicles = (args: VehicleSearchArgs | null) => {
  return useSearch(vehiclesApi.endpoints.getVehiclesByMakeAndModel, args);
};
