export interface Vehicle {
  readonly make: string;
  readonly model: string;
  readonly enginePowerPS: number;
  readonly enginePowerKW: number;
  readonly fuelType: string;
  readonly bodyType: string;
  readonly engineCapacity: number;
}
