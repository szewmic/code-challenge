import React from "react";
import MakesList from "./components/MakesList";
import VehiclesGallery from "./components/VehiclesGallery";
import {
  VehicleSelectorContainer,
  MakesListContainer,
  VehiclesGalleryContainer
} from "./styles";

interface VehicleSelectorProps {
  onSelect?: (data: any) => void;
}

const VehicleSelector = (props: VehicleSelectorProps) => {
  return (
    <VehicleSelectorContainer id="vehicle-selector">
      <MakesListContainer id="vehicle-selector-makes-list">
        <MakesList />
      </MakesListContainer>
      <VehiclesGalleryContainer id="vehicle-selector-vehicles-gallery">
        <VehiclesGallery {...props} />
      </VehiclesGalleryContainer>
    </VehicleSelectorContainer>
  );
};

export default VehicleSelector;
