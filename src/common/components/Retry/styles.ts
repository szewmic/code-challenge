import Styled from "styled-components";

export const RetryButton = Styled.button`
`;

interface RetryMessageProps {
  readonly type: "base" | "dark";
}

export const RetryMessage = Styled.p<RetryMessageProps>`
color: ${(props) => (props.type === "base" ? "white" : "black")};
`;
