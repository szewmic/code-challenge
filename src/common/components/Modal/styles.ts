import Styled, { css } from "styled-components";

const BottomRightPos = css`
  bottom: 15px;
  right: 15px;
  transition: transform 0.6s ease-in-out;
  animation: toast-in-right 0.7s;
`;

export type PositionType = "bottom-right";

const positions = {
  "bottom-right": BottomRightPos
};

interface ModalNotificationProps {
  position: PositionType;
}
export const ModalContainer = Styled.div<ModalNotificationProps>`
    box-sizing: border-box;
    position: fixed;
    width: 40%;
    height: 15vh;
    hax-height: 30vh;
    ${(props) => (positions[props.position] ? positions[props.position] : "")}
`;

export const ModalNotification = Styled.div<ModalNotificationProps>`
display: flex;
justify-content: center;
align-items: center;
background-color: green;
color: white;
width: 100%;
height: 100%;
text-align: center;
font-size: 14px;
${(props) => (positions[props.position] ? positions[props.position] : "")}
`;

export const ModalTitle = Styled.h3`
`;

export const ModalMessage = Styled.p`
`;

export const ModalButton = Styled.button`
margin 5px;
position: absolute;
right: 0px;
`;
