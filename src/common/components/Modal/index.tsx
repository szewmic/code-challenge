import React, { useEffect, useState } from "react";
import {
  ModalContainer,
  ModalMessage,
  ModalNotification,
  ModalTitle,
  PositionType,
  ModalButton
} from "./styles";

export interface ModalNotificationOptions {
  title: string;
  message: string;
}

export interface ModalProps {
  readonly messages: ModalNotificationOptions[];
  readonly position: PositionType;
  readonly autoDeleteTime?: number;
}

const Modal = ({ messages, position, autoDeleteTime }: ModalProps) => {
  const [list, setList] = useState(messages);
  const refreshMessages = () => {
    if (list.length > 0) {
      list.pop();
      setList([...list]);
    }
  };

  useEffect(() => {
    const interval = setInterval(() => {
      refreshMessages();
    }, autoDeleteTime ?? 3000);
    return () => {
      clearInterval(interval);
    };
  }, [list]);

  useEffect(() => {
    setList(messages);
  }, [messages]);

  return (
    <ModalContainer position={position}>
      {list.map((it, index) => {
        return (
          <ModalNotification key={`modal-message-${index}`} position={position}>
            <ModalButton
              onClick={(e) => {
                refreshMessages();
              }}
            >
              X
            </ModalButton>
            <div>
              <ModalTitle>{it.title}</ModalTitle>
              <ModalMessage>{it.message}</ModalMessage>
            </div>
          </ModalNotification>
        );
      })}
    </ModalContainer>
  );
};

export default Modal;
