import Styled, { css } from "styled-components";

export const GalleryContainer = Styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(20rem, 1fr));
  grid-gap: 1.5rem;
  padding: 1.5rem;
  background-color: #F4F7FC;
  height: 100%;
  width: 100%;
  overflow: auto;

  @media (max-width: 34em) {
  max-height: 500px;
}
`;

interface GalleryItemProps {
  selected: boolean;
}

const SelectedVehicle = css`
  border: 2px solid lightgreen;
  border-radius: 10px;
`;

export const GalleryItem = Styled.div<GalleryItemProps>`
  font-family: sans-serif;
  background-color: #262525;
  color: #fff;
  padding: 1.5rem;
  ${(props) => props.selected && SelectedVehicle};
  &:hover {
  cursor: default;
  background-color: #4a4848;
}
`;
