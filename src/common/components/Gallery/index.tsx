import { FunctionComponent, ReactNode, useState } from "react";
import { GalleryContainer, GalleryItem } from "./styles";

export interface GalleryProps {
  readonly data: Array<any>;
  readonly onSelect?: (data: any) => void;
  readonly valueExpr?: string;
  readonly itemRender?: (data: any) => ReactNode;
  readonly ItemComponent?: FunctionComponent<{ data: any }>;
}

const Gallery = (props: GalleryProps) => {
  const { data, ItemComponent, itemRender, onSelect, valueExpr } = props;
  const [selectedItem, setSelectedItem] = useState<any>(null);

  const renderItem = (data: any) => {
    if (ItemComponent) {
      return <ItemComponent {...{ data }} />;
    }

    if (itemRender) {
      return itemRender(data);
    }
    if (valueExpr) {
      return <div>{data[valueExpr] ? data[valueExpr] : ""}</div>;
    }

    return <div>data</div>;
  };

  return (
    <GalleryContainer id="gallery-container">
      {data.map((it, index) => {
        const id = `list-${index}`;
        return (
          <GalleryItem
            selected={id === selectedItem}
            data-testid={id}
            key={id}
            onClick={(e) => {
              if (onSelect) {
                onSelect(it);
              }
              setSelectedItem(id);
            }}
          >
            {renderItem(it)}
          </GalleryItem>
        );
      })}
    </GalleryContainer>
  );
};

export default Gallery;
