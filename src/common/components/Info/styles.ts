import Styled from "styled-components";

export const InfoContainer = Styled.div`
color: white;
display: flex;
justify-content: center;
align-items: center;
width: 100%;
height: 100%;
`;

export const InfoLabel = Styled.span`
flex: 0 0 200px;
`;
