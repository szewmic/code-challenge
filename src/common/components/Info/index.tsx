import React from "react";
import { InfoContainer, InfoLabel } from "./styles";

export interface InfoProps {
  readonly text: string;
}

const Info = ({ text }: InfoProps) => {
  return (
    <InfoContainer>
      <InfoLabel>{text}</InfoLabel>
    </InfoContainer>
  );
};

export default Info;
