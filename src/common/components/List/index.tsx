import React, { FunctionComponent, useEffect, useState } from "react";
import {
  StyledList,
  ListItem,
  ListItemText,
  ListWrapper,
  Title,
  FilterInput
} from "./styles";

export interface ListContentRenderContext {
  readonly selectedItem: any;
}

export interface ListProps {
  readonly data: Array<any>;
  readonly title?: string;
  readonly filteringEnabled?: boolean;
  readonly listContentRender?: (ctx: ListContentRenderContext) => JSX.Element;
  readonly ItemComponent?: FunctionComponent<{ data: any }>;
  readonly onSelect: (selectedItem: any) => void;
}

const List = (props: ListProps) => {
  const {
    data,
    filteringEnabled,
    title,
    onSelect,
    ItemComponent,
    listContentRender
  } = props;
  const [internalData, setInternalData] = useState(data);
  const [filter, setFilter] = useState<string>("");
  const [selectedItem, setSelectedItem] = useState<string | null>(null);

  useEffect(() => {
    if (filter) {
      const filteredData = data.filter((it) =>
        it.toLowerCase().includes(filter.toLowerCase())
      );
      setInternalData(filteredData);
    } else setInternalData(data);
  }, [data, filter]);

  const onFilterInputChange = (e: any) => {
    setFilter(e.target.value);
  };

  const renderItem = (data: any) => {
    if (ItemComponent) {
      return <ItemComponent {...{ data }} />;
    }

    if (listContentRender) {
      return listContentRender(data);
    }

    return <div>data</div>;
  };

  return (
    <ListWrapper>
      {title && (
        <Title id="list-title" data-testid="list-title">
          {title}
        </Title>
      )}
      {filteringEnabled && (
        <FilterInput
          key="list-filter-input"
          data-testid="list-filter-input"
          value={filter}
          onChange={onFilterInputChange}
        ></FilterInput>
      )}
      <StyledList id="list-ul">
        {internalData.map((it, index) => {
          const id = `list-${index}`;
          return (
            <ListItem
              selected={it === selectedItem}
              onClick={(e) => {
                e.stopPropagation();
                if (it === selectedItem) {
                  setSelectedItem(null);
                } else setSelectedItem(it);
                onSelect(it);
              }}
              data-testid={id}
              key={id}
            >
              <ListItemText>{it}</ListItemText>
              {it === selectedItem &&
                (ItemComponent || listContentRender) &&
                renderItem(it)}
            </ListItem>
          );
        })}
      </StyledList>
    </ListWrapper>
  );
};

List.defaultProps = {
  data: [],
  filteringEnabled: false
};

export default List;
