import Styled from "styled-components";

interface ListWrapperProps {
  readonly width?: string | number;
  readonly height?: string | number;
}

export const ListWrapper = Styled.div<ListWrapperProps>`
display: block;
flex-flow: column;
align-items: flex-start;
border-radius: 14px;
transform: rotate(0deg);
padding: 1.5rem;
background-color: #F4F7FC;
width: ${(props) => (props.width ? `${props.width}px` : "100%")};
height: ${(props) => (props.height ? `${props.height}px` : "100%")};
overflow: auto;
@media (max-width: 34em) {
  max-height: 350px;
  text-align: center;
}
`;

export const Title = Styled.h2`
margin: auto;
flex: 0 1 80%;
`;

export const StyledList = Styled.ul`
list-style: none;
height: 100%;
width: 100%;
`;

interface ListItemProps {
  selected: boolean;
}

export const ListItem = Styled.li<ListItemProps>`
padding: 3px 12px;
border: ${(props) => (props.selected ? "1px solid" : "")};
&:hover {
  cursor: default;
  background-color: #e8eaed;
}
`;

export const ListItemText = Styled.span`
font-family: SF Pro Text;
font-size: 13px;
line-height: 15px;
`;

export const FilterInput = Styled.input`
width: 80%;
margin: 5px 0 5px 0;
`;
