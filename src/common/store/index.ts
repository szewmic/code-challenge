import { combineReducers, configureStore } from "@reduxjs/toolkit";
import { MAKES_API_REDUCER_KEY, makesApi } from "../../api/makes/api";
import { MODELS_API_REDUCER_KEY, modelsApi } from "../../api/models/api";
import { VEHICLES_API_REDUCER_KEY, vehiclesApi } from "../../api/vehicles/api";
import { TypedUseSelectorHook, useDispatch, useSelector } from "react-redux";
import VehicleSelectorReducer, {
  vehicleSelectorSlice
} from "../../features/VehicleSelector/slice";

const reducers = {
  [MAKES_API_REDUCER_KEY]: makesApi.reducer,
  [MODELS_API_REDUCER_KEY]: modelsApi.reducer,
  [VEHICLES_API_REDUCER_KEY]: vehiclesApi.reducer,
  [vehicleSelectorSlice.name]: VehicleSelectorReducer
};

const combinedReducer = combineReducers<typeof reducers>(reducers);

export const store = configureStore({
  reducer: combinedReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat([
      makesApi.middleware,
      modelsApi.middleware,
      vehiclesApi.middleware
    ]),
  devTools: true
});

export type RootState = ReturnType<typeof combinedReducer>;
export type AppDispatch = typeof store.dispatch;

export const useTypedDispatch = () => useDispatch<AppDispatch>();
export const useTypedSelector: TypedUseSelectorHook<RootState> = useSelector;
