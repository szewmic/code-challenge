import "./styles.css";
import VehiclesSelector from "./features/VehicleSelector";
import { Modal } from "./common/components";
import { useCallback, useState } from "react";
import { Vehicle } from "./features/VehicleSelector/models/vehicle";
import { ModalNotificationOptions } from "./common/components/Modal";

export default function App() {
  const [modals, setModals] = useState<ModalNotificationOptions[]>([]);
  const onSelect = useCallback((sv: Vehicle) => {
    setModals([
      {
        title: "Selected car",
        message: `${sv.make} ${sv.model} ${sv.enginePowerPS}`
      }
    ]);
  }, []);

  return (
    <div className="App">
      <main>
        <VehiclesSelector onSelect={onSelect} />
      </main>
      <Modal messages={modals} position="bottom-right" />
    </div>
  );
}
