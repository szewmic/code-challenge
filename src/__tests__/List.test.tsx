import React from "react";
import { fireEvent, render } from "@testing-library/react";
import '@testing-library/jest-dom/extend-expect'
import List, { ListProps } from "../common/components/List/index";

const data = ["Apple", "Banana", "Orange"];

const renderList = (props: Partial<ListProps> = {}) => {
  const defaultProps: ListProps = {
    title: "List",
    data,
    onSelect: (newPage: number) => {}
  };
  return render(<List {...defaultProps} {...props} />);
};

describe("<List />", () => {
  test("list order is correct", async () => {
    const { findByTestId } = renderList({ data: data });
    const firstLi = await findByTestId("list-0");
    const lastLi = await findByTestId(`list-${data.length - 1}`);

    expect(firstLi.textContent).toBe(data[0]);
    expect(lastLi.textContent).toBe(data[data.length - 1]);
  });

  test("on click event is invoked", async () => {
    let selectedItem = null;
    const onSelect = (si: any) => {
      selectedItem = si;
    };
    const { findByTestId } = renderList({ data: data, onSelect: onSelect });
    const firstLi = await findByTestId("list-0");
    const lastLi = await findByTestId(`list-${data.length - 1}`);
    fireEvent.click(firstLi);
    expect(selectedItem).toBe(data[0]);
    fireEvent.click(lastLi);
    expect(selectedItem).toBe(data[data.length - 1]);
  });

  test("title is displayed", async () => {
    const title = "Fruits";
    const { findByTestId } = renderList({ data: data, title });
    const titleEl = await findByTestId("list-title");

    expect(titleEl.textContent).toBe(title);
  });

  test("exact match filtering works", async () => {
    const { findByTestId, queryByText } = renderList({
      data: data,
      filteringEnabled: true
    });
    const filterInput = await findByTestId("list-filter-input");
    expect(queryByText(data[0])).toBeInTheDocument();
    expect(queryByText(data[1])).toBeInTheDocument();
    expect(queryByText(data[2])).toBeInTheDocument();

    fireEvent.change(filterInput, { target: { value: data[1] } });

    expect(queryByText(data[1])).toBeInTheDocument();
    expect(queryByText(data[0])).not.toBeInTheDocument();
    expect(queryByText(data[2])).not.toBeInTheDocument();

    fireEvent.change(filterInput, { target: { value: data[0] } });

    expect(queryByText(data[0])).toBeInTheDocument();
    expect(queryByText(data[1])).not.toBeInTheDocument();
    expect(queryByText(data[2])).not.toBeInTheDocument();
  });

  test("filtering shows result where atleast one letter match", async () => {
    const { findByTestId, queryByText } = renderList({
      data: data,
      filteringEnabled: true
    });

    const filterInput = await findByTestId("list-filter-input");
    expect(queryByText(data[0])).toBeInTheDocument();
    expect(queryByText(data[1])).toBeInTheDocument();
    expect(queryByText(data[2])).toBeInTheDocument();

    fireEvent.change(filterInput, { target: { value: "e" } });

    expect(queryByText(data[0])).toBeInTheDocument();
    expect(queryByText(data[1])).not.toBeInTheDocument();
    expect(queryByText(data[2])).toBeInTheDocument();
  });

  test("filtering is case insensitive", async () => {
    const { findByTestId, queryByText } = renderList({
      data: data,
      filteringEnabled: true
    });

    const filterInput = await findByTestId("list-filter-input");
    expect(queryByText(data[0])).toBeInTheDocument();
    expect(queryByText(data[1])).toBeInTheDocument();
    expect(queryByText(data[2])).toBeInTheDocument();

    fireEvent.change(filterInput, { target: { value: "e" } });

    expect(queryByText(data[0])).toBeInTheDocument();
    expect(queryByText(data[1])).not.toBeInTheDocument();
    expect(queryByText(data[2])).toBeInTheDocument();

    fireEvent.change(filterInput, { target: { value: "E" } });

    expect(queryByText(data[0])).toBeInTheDocument();
    expect(queryByText(data[1])).not.toBeInTheDocument();
    expect(queryByText(data[2])).toBeInTheDocument();
  });
});
