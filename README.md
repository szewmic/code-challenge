# Szewmic
## Friday code challenge

Solution to friday code challenge which is selection a vehicle. User has to first select a make then model and after that choose a vehicle. API sometimes throws errors so it has to be handled and give user possibility to try fetch data again.
UI should be build with RWD mobile first approach.

## Features

- Viewing list of available in api makes with filtering
- Viewing list of models by clicking on preffered make
- Displaying list of vehicles after selecting model and make
- After selecting vehicle custom toast is shown
- Server error handled by showing retry button allowing user to refetch data
- RWD approach in building UI

## Tech

Used in solution technologies:

- [React] - ovious one
- [TypeScript] - for safe typing
- [Redux] - global app state management
- [ReduxToolkit] - to avoid writing boilerplatte,
- [Redux Toolkit Query] - to invalidate server data cache and avoid uneccesary fetching data
- [Styled-Components] - to write encapsulated css in js styles, just personal preference
- [CRA] - create react app setup for speeding up development
- [react-testing-library + jest] - for few unit tests


## Installation

Install the dependencies and devDependencies and start the server.

```sh
npm install
npm run start
```



## License

MIT